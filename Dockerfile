FROM python:3.6

COPY requirements.txt /app/
WORKDIR /app

RUN pip3 install --upgrade pip 
RUN pip3 install -r requirements.txt               

COPY vcf_insert_db.py /app/
COPY populate_gnomad_common_db.py /app/
COPY runner.py /app/

ENTRYPOINT [ "python", "/app/runner.py" ]
