import os
import pymysql
import hashlib
import sys
import json
import zlib

bucket = os.getenv('GNOMAD_COMMON_BUCKET')
object = os.getenv('GNOMAD_COMMON_OBJECT')
gnomad_common_md5 = os.getenv('GNOMAD_COMMON_MD5')
mysql_host = os.getenv('GNOMAD_COMMON_MYSQL_HOST') or os.getenv('MYSQL_HOST')
mysql_username = os.getenv('GNOMAD_COMMON_MYSQL_USERNAME')
mysql_password = os.getenv('GNOMAD_COMMON_MYSQL_PASSWORD')
mysql_database = os.getenv('GNOMAD_COMMON_MYSQL_DATABASE') or os.getenv('MYSQL_DATABASE')


def populate_gnomad_common_db(boto3client):
  global bucket
  global object
  filename = '/tmp/gnomad_common.json.zlib'
  lovd_db = pymysql.connect(mysql_host, mysql_username, mysql_password, mysql_database)
  lovd_c = lovd_db.cursor()
  q = "SHOW TABLES LIKE 'gnomad_common'"
  lovd_c.execute(q)
  if not lovd_c.fetchone():
    try:
      bucket = boto3client.Bucket(bucket)
      object = bucket.Object(object)
      object.download_file(filename)
    except Exception as err:
      print('Error while downloading gnomad common file: ${err}', file=sys.stderr)
      sys.exit(4)

    md5_hash = hashlib.md5()
    with open(filename,"rb") as f:
      for byte_block in iter(lambda: f.read(4096),b""):
        md5_hash.update(byte_block)
    if md5_hash.hexdigest() != gnomad_common_md5:
      print('Invalid gnomad common file', file=sys.stderr)
      sys.exit(3)

    q = """CREATE TABLE gnomad_common (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  variants VARCHAR(1000),
  PRIMARY KEY (id),
  INDEX variants_index(variants)
);"""
    lovd_c.execute(q)
    lovd_db.commit()

    with open(filename, 'rb') as f:
      variants = f.read()
    variants = json.loads(zlib.decompress(variants).decode('utf-8'))
    q = "INSERT INTO gnomad_common (variants) VALUES (%s)"
    lovd_c.executemany(q, variants)
    lovd_db.commit()
